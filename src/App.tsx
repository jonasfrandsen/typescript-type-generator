import React, { useState } from "react";
import { InputFields, Output } from "./components";
import { v4 as uuidv4 } from "uuid";

export interface IInputField {
  id: string;
  input: string;
  select: string;
  object: any[];
  array: any[];
}

const App: React.FC = () => {
  const inputFieldObject: IInputField = {
    id: uuidv4(),
    input: "",
    select: "string",
    object: [],
    array: [],
  };

  const [data, setData] = useState([{ ...inputFieldObject }]);

  return (
    <div style={{ width: "100%", display: "flex", flexDirection: "column" }}>
      <h1 style={{ display: "flex", marginLeft: "1rem" }}>
        Typescript type generator
      </h1>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "flex-start",
        }}
      >
        {/* Rekursive komponenter */}
        {data.map((input) => (
          <InputFields
            key={`input-field-${input.id}`}
            {...input}
            data={data}
            setData={setData}
          />
        ))}
        {data.map((input) => (
          <Output key={`output-field-${input.id}`} {...input} data={data} />
        ))}
      </div>
    </div>
  );
};

export default App;
