import { IInputField } from "./App";

function findChildInData(obj: IInputField, targetId: string | null): any {
  if (obj.id === targetId) {
    return obj;
  }
  if (obj.object) {
    for (let item of obj.object) {
      let check = findChildInData(item, targetId);
      if (check) {
        return check;
      }
    }
  }

  if (obj.array) {
    for (let item of obj.array) {
      let check = findChildInData(item, targetId);
      if (check) {
        return check;
      }
    }
  }

  return null;
}

function findParentId(target: HTMLElement | null) {
  let parentElement = target;

  while (parentElement && !parentElement.hasAttribute("id")) {
    parentElement = parentElement.parentElement;
  }

  let targetId: string | null = parentElement
    ? parentElement.getAttribute("id")
    : "";

  return targetId;
}

export { findChildInData, findParentId };
