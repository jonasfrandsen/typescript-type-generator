import React from "react";
import { IInputField } from "../App";

export interface Props {
  data: any[];
  id: string;
}

const Output: React.FC<Props> = ({ id, data }) => {
  const outputTemplate = (obj: IInputField) => {
    let output = "";

    for (let property in obj) {
      if (obj.select === "object") {
        if (property === "id") {
          const objMap = obj.object.map(
            (item) => `${item.input}: ${item.select}`
          );
          output += `type ${obj.input} = {
            ${objMap}
          }`;
        }
      }
      if (obj.select === "array") {
        for (let prop in obj.array) {
          if (obj.array[prop].select === "object") {
            return (output += `type ${obj.input} = Array<{${obj.array[
              prop
            ].object.map((item: any) => `${item.input}: ${item.select}`)}}>`);
          } else if (obj.array[prop].select === "array") {
            return (output += `type ${obj.input} = Array<${obj.array[prop].select}>`);
          } else {
            return (output += `type ${obj.input} = Array<${obj.array[prop].select}>`);
          }
        }
      }
      if (obj.select === "string" || obj.select === "number") {
        if (property === "select") {
          output += `type ${obj.input} = ${obj.select}`;
        }
      }
    }
    return output;
  };

  return (
    <div
      id={id}
      style={{
        display: "flex",
        flexDirection: "column",
        width: "50%",
        marginLeft: "1rem",
      }}
    >
      <pre>{outputTemplate(data[0])}</pre>
    </div>
  );
};

export default Output;
