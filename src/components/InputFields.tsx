import React from "react";
import { v4 as uuidv4 } from "uuid";
import { IInputField } from "../App";
import { findChildInData, findParentId } from "../functions";
import cloneDeep from "lodash.clonedeep";

//select element valgmuligheder
const options = [
  {
    id: 1,
    value: "string",
  },
  {
    id: 2,
    value: "number",
  },
  {
    id: 3,
    value: "array",
  },
  {
    id: 4,
    value: "object",
  },
];

interface Props {
  data: any[];
  setData: React.Dispatch<React.SetStateAction<any>>;
  id: string;
  input: string;
  select: string;
  object: any[];
  array: any[];
}

const InputFields: React.FC<Props> = ({
  id,
  input,
  select,
  array,
  object,
  data,
  setData,
}) => {
  const inputFieldObject: IInputField = {
    id: uuidv4(),
    input: "",
    select: "string",
    object: [],
    array: [],
  };

  const handleInputChange = (
    event: React.FormEvent<HTMLInputElement>
  ): void => {
    let target = event.currentTarget;

    //Find ID på parent element og gem i targetId
    let targetId = findParentId(target);

    //Klon af data state
    let arrayClone = cloneDeep(data);

    //Find aktuelt objekt i klon af data state og gem i child
    let child: IInputField | undefined = undefined;
    for (let obj of arrayClone) {
      child = findChildInData(obj, targetId);
      if (child) {
        break;
      }
    }

    //Tilføj ny værdi fra onChange event til objekt
    if (child) {
      child.input = target.value;
    }

    //Opdatér data state med ændringer
    setData(arrayClone);
  };

  const handleOptionChange = (
    event: React.FormEvent<HTMLSelectElement>
  ): void => {
    let target = event.currentTarget;

    //Find ID på parent element og gem i targetId
    let targetId = findParentId(target);

    //Klon af data state
    let newArrClone = cloneDeep(data);

    //Find aktuelt objekt i klon af data state og gem i child
    let child: IInputField | undefined = undefined;
    for (let obj of newArrClone) {
      child = findChildInData(obj, targetId);
      if (child) {
        break;
      }
    }

    //Tilføj ny værdi fra onChange event til objekt
    if (child) {
      child.select = target.value;

      //Hvis Type array er valgt tilføj nyt input felt til array
      //Fjern eksisterende array hvis en anden Type end array vælges
      if (target.value === "array") {
        if (child.object.length) {
          child.object.splice(0, child.object.length);
        }
        child.array.push(inputFieldObject);
      } else {
        child.array.splice(0, 1);
        child.object.splice(0, child.object.length);
      }
    }

    //Opdatér data state med ændringer
    setData(newArrClone);
  };

  const handleAddObjectProperty = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    event.preventDefault();
    let target = event.currentTarget;

    //Find ID på parent element og gem i targetId
    let targetId = findParentId(target);

    //Klon af data state
    let newArrClone = cloneDeep(data);

    //Find aktuelt objekt i klon af data state og gem i child
    let child: IInputField | undefined = undefined;
    for (let obj of newArrClone) {
      child = findChildInData(obj, targetId);
      if (child) {
        break;
      }
    }

    //Tilføj nyt objekt til nuværende objekt
    if (child) {
      child.object.push(inputFieldObject);
    }

    //Opdatér data state med ændringer
    setData(newArrClone);
  };

  const handleRemoveElementFromObject = (
    event: React.MouseEvent<HTMLButtonElement>,
    index: number
  ) => {
    let target = event.currentTarget;

    //Find ID på parent element og gem i targetId
    let targetId = findParentId(target);

    //Klon af data state
    let newArrClone = cloneDeep(data);

    //Find aktuelt objekt i klon af data state og gem i child
    let child: IInputField | undefined = undefined;
    for (let obj of newArrClone) {
      child = findChildInData(obj, targetId);
      if (child) {
        break;
      }
    }

    console.log(child);

    //Fjern objekt fra child
    if (child) {
      child.object.splice(index, 1);
    }

    //Opdatér data state med ændringer
    setData(newArrClone);
  };

  return (
    <div
      id={id}
      style={{
        display: "flex",
        flexDirection: "column",
        marginLeft: "1rem",
      }}
    >
      <div style={{ display: "flex", marginBottom: "0.25rem" }}>
        <label style={{ display: "flex" }}>
          Name:
          <input
            type="text"
            name="input"
            value={input}
            onChange={(event) => handleInputChange(event)}
            style={{ marginLeft: "1ch" }}
          />
        </label>
        <label style={{ display: "flex", marginLeft: "1ch" }}>
          Type:
          <select
            value={select}
            onChange={(event) => handleOptionChange(event)}
            style={{ marginLeft: "1ch" }}
          >
            {options.map((option) => {
              return (
                <option key={`option-${option.id}`} value={option.value}>
                  {option.value}
                </option>
              );
            })}
          </select>
        </label>
      </div>
      {array.map((arr: IInputField, ind) => (
        <InputFields
          key={`array-elements${arr.id}`}
          {...arr}
          data={data}
          setData={setData}
        />
      ))}
      {object.map((arr: IInputField, ind) => (
        <div
          key={`object-element${arr.id}`}
          style={{ display: "flex", justifyContent: "flex-start" }}
        >
          <InputFields {...arr} data={data} setData={setData} />
          {object.length ? (
            <button
              onClick={(event) => handleRemoveElementFromObject(event, ind)}
              style={{ display: "flex", width: "fit-content" }}
            >
              -
            </button>
          ) : null}
        </div>
      ))}
      {select === "object" && (
        <button
          onClick={(event) => handleAddObjectProperty(event)}
          style={{
            display: "flex",
            width: "fit-content",
            marginLeft: "1rem",
          }}
        >
          +
        </button>
      )}
    </div>
  );
};

export default InputFields;
